"use strict"

let express = require('express');
let multer = require("multer")
let upload = multer()

let app = express();

app.get('/', (req,res) => {
    res.set('Content-Type', 'text/html');
    res.sendFile(process.cwd()+'/app/index.html');
});

app.post('/submit', upload.single('file'), (req,res) => {
    console.log(req.body)
    console.log(req.file);
    let resJSON = {
        size: req.file.size
    }
    res.set('Content-Type', 'application/json')
    res.send(resJSON);
})

app.listen(process.env.PORT, process.env.IP);